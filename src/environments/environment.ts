// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false, 
  mapbox: {
    accessToken: 'pk.eyJ1Ijoid2FlbHJhYmhpd2FlbCIsImEiOiJjazRkM2JqcXIwdG0xM25vZ2ZiN3F3NGtrIn0.f7xudT3HuGKHLTe63rThWQ'
  }
};

export const URL = {
  getStocks: 'mediset/stockPharmacie/{0}',// id
  addStock: 'mediset/addStock/{0}/{1}',// {id_ph}/{id_med}/{nb_stock}
  updateStock: 'mediset/updateStock/{0}/{1}',// {id_ph}/{id_med}/{nb_stock}
  filterPharmacieByMedicament: 'mediset/filterPharmacieByMedicament/{0}',// {id_med}
  isMedicamentDispo: 'mediset/isMedicamentDispo/{0}/{1}',// {id_med}/{id_ph}
  searchMedicament: 'mediset/search/{0}',// {input}
  getAllMedicament: 'mediset/allMedicament',
  addClient: 'mediset/addClient',
  addPharmacie: 'mediset/addPharmacie', // POST body ; pharmacie
  getPharmacie: 'mediset/getPharmacie/{0}',// {id}
  getAllPharmacie: 'mediset/getAllPharmacie',
  getReportsByPharmacie: 'mediset/getReportsByPharmacie/{0}',// {id}
  addReport: 'mediset/report/{0}/{1}/{2}',// {pharmacie_id}/{client_id}/{medicament_id}
  login: 'mediset/login/{0}/{1}' // {userName}/{password}
}