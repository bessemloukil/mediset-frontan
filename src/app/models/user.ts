import { Client } from './client';
import { Pharmacie } from './pharmacie';
import { GpsCoordinate } from './gps-coordinates';
export interface User {
    id: number;
    userName: string;
    phoneNumber: number;
    email: string;

}
export class UserBuilder {
    private id: number = 0;
    private userName: string;
    private phoneNumber: number;
    private mail: string;
    private isNight: boolean;
    private nextGuardDate: Date;
    private numberWeekToGuard: number;
    private address: GpsCoordinate;
    private birthDay: Date;
    constructor() {

    }
    setUserName(userName): UserBuilder {
        this.userName = userName;
        return this;
    }
    setMail(mail): UserBuilder {
        this.mail = mail;
        return this;
    }
    setPhoneNumber(phoneNumber): UserBuilder {
        this.phoneNumber = phoneNumber;
        return this;
    }
    setNight(isNight): UserBuilder {
        this.isNight = isNight;
        return this;
    }
    setNextGuardDate(nextGuardDate): UserBuilder {
        this.nextGuardDate = nextGuardDate;
        return this;
    }
    setAddress(address): UserBuilder {
        this.address = address;
        return this;
    }
    setNumberWeekToGuard(numberWeekToGuard): UserBuilder {
        this.numberWeekToGuard = numberWeekToGuard;
        return this;
    }
    setBirthDay(birthDay): UserBuilder {
        this.birthDay = birthDay;
        return this;
    }
    buildPharmacie(): Pharmacie {
        return new Pharmacie(
            this.id,
            this.userName,
            this.phoneNumber,
            this.mail,
            this.isNight,
            this.nextGuardDate,
            this.numberWeekToGuard,
            this.address
        )
    }
    buildClient(): Client {
        return new Client(
            this.id,
            this.userName,
            this.phoneNumber,
            this.mail,
            this.birthDay
        )
    }
}