import { Medicament } from './medicament';
export class Stock {
    id: number;
    medicament: Medicament;
    quantity: number;
}