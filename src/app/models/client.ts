import { User } from './user';

export class Client implements User {
    constructor(
        public id: number,
        public userName: string,
        public phoneNumber: number,
        public email: string,
        public birthDay: Date,
    ) {
    }
}