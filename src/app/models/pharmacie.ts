import { GpsCoordinate } from './gps-coordinates';
import { User } from './user';
export class Pharmacie implements User {
    constructor(
        public id: number,
        public userName: string,
        public phoneNumber: number,
        public email: string,
        public isNight: boolean,
        public nextGuardDate: Date,
        public numberWeekToGuard: number,
        public adresse_id: GpsCoordinate,
    ) {

    }
}