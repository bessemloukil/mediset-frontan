import { TypeMedicament } from './type-medicament';
export class Medicament {
    id: number;
    name: string;
    type: TypeMedicament;
    nbUnite: number;
    unite: string;
    prix: number;
}