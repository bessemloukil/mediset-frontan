import { Stock } from './stock';
import { Client } from './client';
export class Volition {
    client: Client;
    stock: Stock[];
}