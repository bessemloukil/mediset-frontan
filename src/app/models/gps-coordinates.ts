export class GpsCoordinate {
    lat: number;
    lng: number;
}