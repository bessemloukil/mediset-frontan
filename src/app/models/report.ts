import { Medicament } from './medicament';
import { Pharmacie } from './pharmacie';
import { Client } from './client';
export class Report {
    id: number;
    client: Client;
    pharmacie: Pharmacie;
    medicament: Medicament;
}