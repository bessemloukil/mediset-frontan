import { PharmacieService } from './../services/pharmacie.service';
import { Pharmacie } from './../models/pharmacie';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-pharmacie-profile',
  templateUrl: './pharmacie-profile.component.html',
  styleUrls: ['./pharmacie-profile.component.scss']
})
export class PharmacieProfileComponent implements OnInit {


  pharmacie: Pharmacie;
  constructor(
    private route: ActivatedRoute,
    private pharmacieService: PharmacieService
  ) { }

  ngOnInit() {
    this.pharmacieService.getPharmacie(this.route.snapshot.params['id']).subscribe(ph => {
      this.pharmacie = ph;
    })

  }

}
