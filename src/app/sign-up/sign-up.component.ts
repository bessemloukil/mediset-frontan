import { Pharmacie } from './../models/pharmacie';
import { GpsCoordinate } from './../models/gps-coordinates';
import { AuthentificationService } from 'app/login/authentification.service';
import { UserBuilder } from './../models/user';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {

  isClient = true;
  form: FormGroup;
  userBuilder = new UserBuilder();
  constructor(
    private formBuilder: FormBuilder,
    private authentificationService: AuthentificationService,
    private router: Router
  ) { }
  pharmacieLocation: Pharmacie[] = []
  ngOnInit() {
    this.form = this.formBuilder.group({
      userName: [''],
      phoneNumber: [''],
      mail: [''],
      isNight: [''],
      nextGuardDate: [''],
      numberWeekToGuard: [''],
      address: [''],
      birthDay: [''],
      password: [''],
      lng: [''],
      lat: ['']
    });
    this.form.controls.userName.valueChanges.subscribe(v => {
      this.userBuilder.setUserName(v);
    });
    this.form.controls.phoneNumber.valueChanges.subscribe(v => {
      this.userBuilder.setPhoneNumber(v);
    });
    this.form.controls.mail.valueChanges.subscribe(v => {
      this.userBuilder.setMail(v);
    });
    this.form.controls.isNight.valueChanges.subscribe(v => {
      this.userBuilder.setNight(v);
    });
    this.form.controls.nextGuardDate.valueChanges.subscribe(v => {
      this.userBuilder.setNextGuardDate(v);
    });
    this.form.controls.address.valueChanges.subscribe(v => {
      this.userBuilder.setAddress(v);
    });
    this.form.controls.birthDay.valueChanges.subscribe(v => {
      this.userBuilder.setBirthDay(v);
    });
    this.form.controls.numberWeekToGuard.valueChanges.subscribe(v => {
      this.userBuilder.setNumberWeekToGuard(v);
    });
    this.form.controls.lng.valueChanges.subscribe(v => {
      if (v && this.form.controls.lat.value) {
        this.form.controls.address.setValue({ lng: this.form.controls.lng.value, lat: this.form.controls.lat.value })
      }
    });
    this.form.controls.lat.valueChanges.subscribe(v => {
      if (v && this.form.controls.lat.value) {
        this.form.controls.address.setValue({ lng: this.form.controls.lng.value, lat: this.form.controls.lat.value })
      }
    });

  }

  savePharmacie() {
    console.log(this.userBuilder.buildPharmacie());
    this.authentificationService.signIn(this.userBuilder.buildPharmacie(), this.form.controls.password.value)
      .subscribe(r => {
        this.router.navigate(['/login']);
      });


  }
  saveClient() {
    this.authentificationService.signIn(this.userBuilder.buildClient(), this.form.controls.password.value)
      .subscribe(r => {
        this.router.navigate(['/login']);
      });
  }
  mapClick(cord: GpsCoordinate) {
    this.form.controls.lat.setValue(cord.lat);
    this.form.controls.lng.setValue(cord.lng);

    this.pharmacieLocation = [
      new UserBuilder().setAddress(cord).setUserName('Position').buildPharmacie()
    ]

  }
}
