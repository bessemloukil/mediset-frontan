import { Router } from '@angular/router';
import { AuthentificationService } from './authentification.service';
import { Component, OnInit } from '@angular/core';
import { Pharmacie } from 'app/models/pharmacie';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  userName: string;
  password: string;
  constructor(private authentification: AuthentificationService, private router: Router) { }

  ngOnInit() {
  }
  login() {
    this.authentification.login(this.userName, this.password).subscribe(r => {
      if (r) {
        localStorage.setItem('user', JSON.stringify(r));
        
        if((r as Pharmacie).nextGuardDate){
          this.router.navigate(['stock']);
        }else this.router.navigate(['maps']);

      } else {
        alert('username or password invalid');
      }
    })

  }

}
