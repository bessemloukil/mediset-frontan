import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
@Injectable()
export class AuthReverseGuardService implements CanActivate {
  constructor(public router: Router) { }
  canActivate(): boolean {
    if (localStorage.getItem('user')) {
      this.router.navigate(['dashboard']);
      return false;
    }
    return true;
  }
}