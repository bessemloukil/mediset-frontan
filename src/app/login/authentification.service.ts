import { Client } from './../models/client';
import { Md5 } from 'ts-md5/dist/md5';
import { Pharmacie } from './../models/pharmacie';
import { User } from './../models/user';
import { Injectable } from '@angular/core';
import { of, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { StringFormat } from 'app/resources';
import { URL } from 'environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthentificationService {

  constructor(private httpClient: HttpClient) { }
  login(userName: string, password: string): Observable<User> {
    // if (userName == 'admin' && password == "admin") {
    //   return of({
    //     id: 1,
    //     userName: 'string',
    //     phoneNumber: 0,
    //     mail: 'string',
    //     birthDay: new Date(),
    //   })
     

    // }
    // return of(null)
    password = Md5.hashAsciiStr(password) as string;
    console.log('password', password);


    return this.httpClient.get<User>(StringFormat(URL.login, userName, password));

  }
  signIn(user: User, password: string): Observable<User> {

    password = Md5.hashAsciiStr(password) as string;
    console.log('password', password);
    if (user instanceof Client) {
      return this.httpClient.post<User>(URL.addClient + '/' + password, user);
    } else {
      return this.httpClient.post<User>(URL.addPharmacie + '/' + password, user);
    }

  }
  getCurrentUser(): User {
    return JSON.parse(localStorage.getItem('user'));
  }

}
