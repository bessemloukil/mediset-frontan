import { UserBuilder } from './../models/user';
import { Pharmacie } from './../models/pharmacie';
import { GpsCoordinate } from './../models/gps-coordinates';
import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import * as mapboxgl from 'mapbox-gl';
import { environment } from 'environments/environment';
import { GeoJson } from './map';
declare const google: any;

interface Marker {
    lat: number;
    lng: number;
    label?: string;
    draggable?: boolean;
}
@Component({
    selector: 'app-maps',
    templateUrl: './maps.component.html',
    styleUrls: ['./maps.component.css']
})
export class MapsComponent implements OnInit, OnChanges {
    ngOnChanges(changes: SimpleChanges): void {
        this.drawMarkers();
    }


    map: mapboxgl.Map;
    style = 'mapbox://styles/mapbox/streets-v11';

    // tslint:disable:member-ordering
    @Input() width: string = '100%';
    @Input() height: string = '100%';

    @Input() center: GpsCoordinate = {
        lat: 37.76342,
        lng: -122.42742,
    }

    @Input() pharmacies: Pharmacie[] = [];
    @Output() locationClick = new EventEmitter<GpsCoordinate>();
    @Output() pharmacieClick = new EventEmitter<Pharmacie>();

    geoJson: { type: string; geometry: { type: string; coordinates: number[]; }; properties: { message: string; }; }[];

    getPosition(): Promise<any>
    {
      return new Promise((resolve, reject) => {
  
        navigator.geolocation.getCurrentPosition(resp => {
  
            resolve({lng: resp.coords.longitude, lat: resp.coords.latitude});
          },
          err => {
            reject(err);
          });
      });
  
    }
    
    constructor() {
        (mapboxgl as any).accessToken = 'pk.eyJ1Ijoid2FlbHJhYmhpd2FlbCIsImEiOiJjazRkM2JqcXIwdG0xM25vZ2ZiN3F3NGtrIn0.f7xudT3HuGKHLTe63rThWQ';
        
    }

    ngOnInit() {

        console.log('phggg', this.pharmacies);


        this.map = new mapboxgl.Map({
            container: 'map',
            style: this.style,
            zoom: 13,
            center: [this.center.lng, this.center.lat]
        });
        this.map.addControl(new mapboxgl.NavigationControl());


        this.drawMarkers();


        this.map.on('click', (event) => {
            this.locationClick.emit(event.lngLat)
        })


        var size = 200;
        var pulsingDot = {
            width: size,
            height: size,
            data: new Uint8Array(size * size * 4),

            // get rendering context for the map canvas when layer is added to the map
            onAdd: function () {
                var canvas = document.createElement('canvas');
                canvas.width = this.width;
                canvas.height = this.height;
                this.context = canvas.getContext('2d');
            },

            // called once before every frame where the icon will be used
            render: function () {
                var duration = 1000;
                var t = (performance.now() % duration) / duration;

                var radius = (size / 2) * 0.3;
                var outerRadius = (size / 2) * 0.7 * t + radius;
                var context = this.context;

                // draw outer circle
                context.clearRect(0, 0, this.width, this.height);
                context.beginPath();
                context.arc(
                    this.width / 2,
                    this.height / 2,
                    outerRadius,
                    0,
                    Math.PI * 2
                );
                context.fillStyle = 'rgba(255, 200, 200,' + (1 - t) + ')';
                context.fill();

                // draw inner circle
                context.beginPath();
                context.arc(
                    this.width / 2,
                    this.height / 2,
                    radius,
                    0,
                    Math.PI * 2
                );
                context.fillStyle = 'rgba(255, 100, 100, 1)';
                context.strokeStyle = 'white';
                context.lineWidth = 2 + 4 * (1 - t);
                context.fill();
                context.stroke();

                // update this image's data with data from the canvas
                this.data = context.getImageData(
                    0,
                    0,
                    this.width,
                    this.height
                ).data;
                return true;
            }
        };


        this.map.on('load', (event) => {
            this.map.addImage('pulsing-dot', pulsingDot, { pixelRatio: 2 });
            this.map.addSource('customMarker', {
                type: 'geojson',
                data: {
                    type: 'FeatureCollection',
                    features: []
                }
            });

            const data = {
                type: 'FeatureCollection',
                features: this.geoJson
            };
            (this.map.getSource('customMarker') as any).setData(data);

            this.map.addLayer({
                id: 'markers',
                source: 'customMarker',
                type: 'symbol',
                layout: {
                    'text-field': '{message}',
                    'text-size': 24,
                    'text-transform': 'uppercase',
                    'icon-image': 'pulsing-dot',
                    'text-offset': [0, 1.5]
                },
                paint: {
                    'text-color': '#f16624',
                    'text-halo-color': '#fff',
                    'text-halo-width': 2
                }
            });

        });

        const self = this;
        this.map.on('click', 'markers', function (e) {
            self.pharmacieClick.emit(self.getPharmacie(e.lngLat));
        });


    }
    drawMarkers() {
        this.geoJson = this.pharmacies
            .filter(ph => ph.adresse_id)
            .map(ph => {
                return {
                    type: 'Feature',
                    geometry: {
                        type: 'Point',
                        coordinates: [ph.adresse_id.lng, ph.adresse_id.lat]
                    },
                    properties: {
                        message: ph.userName
                    }
                }
            });
        if (!this.geoJson) {
            this.geoJson = []
        }
        if (this.map && this.map.getSource('customMarker')) {
            (this.map.getSource('customMarker') as any).setData({
                type: 'FeatureCollection',
                features: this.geoJson
            });
        }
    }

    getPharmacie(cord: GpsCoordinate): Pharmacie {
        let pharmacie: Pharmacie;
        let distance: number = Number.MAX_VALUE;
        this.pharmacies.forEach(p => {
            if ((Math.abs(p.adresse_id.lat - cord.lat) + Math.abs(p.adresse_id.lng - cord.lng)) / 2 < distance) {
                distance = (Math.abs(p.adresse_id.lat - cord.lat) + Math.abs(p.adresse_id.lng - cord.lng)) / 2;
            }
            pharmacie = p;
        });

        return pharmacie

    }

}
