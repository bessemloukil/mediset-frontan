import { Stock } from './../models/stock';
import { UserBuilder } from './../models/user';
import { TypeMedicament } from './../models/type-medicament';
import { Medicament } from '../models/medicament';
import { Pharmacie } from '../models/pharmacie';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { URL } from 'environments/environment';
import { StringFormat } from 'app/resources';

@Injectable({
  providedIn: 'root'
})
export class StockService {

  constructor(private httpClient: HttpClient) { }
  getStock(pharmacie: Pharmacie): Observable<Stock[]> {
    return this.httpClient.get<Stock[]>(StringFormat(URL.getStocks, '' + pharmacie.id));

  }
  addStock(pharmacie: Pharmacie, medicament: Medicament, nbStock: number): Observable<Stock[]> {
    console.log('phar', pharmacie);
    console.log('med', medicament);
    console.log('nbStock', nbStock);
    return this.httpClient.post<Stock[]>(StringFormat(URL.addStock, '' + pharmacie.id, '' + nbStock), medicament);
  }
  updateStock(stock: Stock, value: number): Observable<number> {
    return this.httpClient.put<number>(StringFormat(URL.updateStock, '' + stock.id, '' + value), null);
  }

  filterPharmacieByMedicament(medicament: Medicament): Observable<Pharmacie[]> {
    return this.httpClient.get<Pharmacie[]>(StringFormat(URL.filterPharmacieByMedicament, '' + medicament.id));
  }
}
