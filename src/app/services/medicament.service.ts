import { TypeMedicament } from './../models/type-medicament';
import { Observable, of } from 'rxjs';
import { Pharmacie } from './../models/pharmacie';
import { Medicament } from './../models/medicament';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { URL } from 'environments/environment';
import { StringFormat } from 'app/resources';

@Injectable({
  providedIn: 'root'
})
export class MedicamentService {

  constructor(private httpClient: HttpClient) { }

  isMedicamentDispo(pharmacie: Pharmacie, medicament: Medicament): Observable<boolean> {
    return this.httpClient.get<boolean>(StringFormat(URL.isMedicamentDispo, pharmacie.id, medicament.id));
  }
  search(input: string): Observable<Medicament[]> {
    return this.httpClient.get<Medicament[]>(StringFormat(URL.searchMedicament, input));
  }
}
