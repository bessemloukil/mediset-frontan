import { UserBuilder } from './../models/user';
import { Pharmacie } from './../models/pharmacie';
import { Observable, of } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { URL } from 'environments/environment';
import { StringFormat } from 'app/resources';

@Injectable({
  providedIn: 'root'
})
export class PharmacieService {

  constructor(private httpClient: HttpClient) { }
  getPharmacie(id: number): Observable<Pharmacie> {
    return this.httpClient.get<Pharmacie>(StringFormat(URL.getPharmacie, '' + id));
  }
  getAllPharmacie(): Observable<Pharmacie[]> {
    return this.httpClient.get<Pharmacie[]>(URL.getAllPharmacie);

  }

}
