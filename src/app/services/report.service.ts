import { Client } from './../models/client';
import { Medicament } from './../models/medicament';
import { Report } from './../models/report';
import { Observable, of } from 'rxjs';
import { Pharmacie } from './../models/pharmacie';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { StringFormat } from 'app/resources';
import { URL } from 'environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ReportService {

  constructor(private httpClient: HttpClient) { }
  getReportsByPharmacie(pharmacie: Pharmacie): Observable<Report[]> {
    return this.httpClient.get<Report[]>(StringFormat(URL.getPharmacie, pharmacie));
  }
  report(client: Client, pharmacie: Pharmacie, medicament: Medicament): Observable<any> {
    return this.httpClient.get<any>(StringFormat(URL.addReport, client.id, pharmacie.id, medicament.id));
  }
}
