import { PharmacieProfileComponent } from './../../pharmacie-profile/pharmacie-profile.component';
import { SearchMedicamentComponent } from './../../search-medicament/search-medicament.component';
import { TableModule } from 'primeng/table';
import { StockManagementComponent } from './../../stock-management/stock-management.component';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminLayoutRoutes } from './admin-layout.routing';
import { DashboardComponent } from '../../dashboard/dashboard.component';
import { UserProfileComponent } from '../../user-profile/user-profile.component';
import { TableListComponent } from '../../table-list/table-list.component';
import { TypographyComponent } from '../../typography/typography.component';
import { IconsComponent } from '../../icons/icons.component';
import { NotificationsComponent } from '../../notifications/notifications.component';
import { UpgradeComponent } from '../../upgrade/upgrade.component';
import { GMapModule } from 'primeng/gmap';
import { AutoCompleteModule } from 'primeng/autocomplete';

import {
  MatButtonModule,
  MatInputModule,
  MatRippleModule,
  MatFormFieldModule,
  MatTooltipModule,
  MatSelectModule,
  MatAutocompleteModule,
  MatProgressSpinnerModule
} from '@angular/material';
import { ComponentsModule } from 'app/components/components.module';
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatRippleModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatTooltipModule,
    GMapModule,
    TableModule,
    AutoCompleteModule,
    MatAutocompleteModule,
    MatProgressSpinnerModule,
    ComponentsModule

  ],
  declarations: [
    DashboardComponent,
    PharmacieProfileComponent,
    UserProfileComponent,
    TableListComponent,
    TypographyComponent,
    IconsComponent,
    NotificationsComponent,
    UpgradeComponent,
    StockManagementComponent,
    SearchMedicamentComponent

  ]
})

export class AdminLayoutModule { }
