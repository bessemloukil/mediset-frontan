export function StringFormat(myString: string, ...args: any[]): string {

    args = args.filter(x => typeof x === 'string');
    let res = myString;
    if (args != undefined && args != null && args.length > 0) {
        args.forEach((x, i) => {
            res = res.replace('{' + i + '}', '' + x);
        });
    }
    return res;
}