import { Component, OnInit } from '@angular/core';
import { Pharmacie } from 'app/models/pharmacie';

declare const $: any;
declare interface RouteInfo {
  path: string;
  title: string;
  icon: string;
  class: string;
}
export const ROUTES: RouteInfo[] = [
  { path: '/stock', title: 'Gestion de stock', icon: 'content_paste', class: '' },
  { path: '/maps', title: 'Maps', icon: 'location_on', class: '' },

];

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  menuItems: any[];

  constructor() { }

  ngOnInit() {
    //localStorage.setItem('user'
    
    var obj = JSON.parse(localStorage.getItem('user'));
    if(( obj as Pharmacie).nextGuardDate){
      const ROUTESs: RouteInfo[] = [
        { path: '/stock', title: 'Gestion de stock', icon: 'content_paste', class: '' },
        { path: '/maps', title: 'Maps', icon: 'location_on', class: '' },
      
      ];
      this.menuItems = ROUTESs.filter(menuItem => menuItem);
    }else {
      const ROUTESs: RouteInfo[] = [
        { path: '/maps', title: 'Maps', icon: 'location_on', class: '' },
      
      ];
      this.menuItems = ROUTESs.filter(menuItem => menuItem);
    }
    
    
  }
  isMobileMenu() {
    if ($(window).width() > 991) {
      return false;
    }
    return true;
  };
}
