import { Router } from '@angular/router';
import { StockService } from './../services/stock.service';
import { Medicament } from './../models/medicament';
import { AuthentificationService } from 'app/login/authentification.service';
import { MedicamentService } from './../services/medicament.service';
import { GpsCoordinate } from './../models/gps-coordinates';
import { PharmacieService } from './../services/pharmacie.service';
import { Pharmacie } from './../models/pharmacie';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-search-medicament',
  templateUrl: './search-medicament.component.html',
  styleUrls: ['./search-medicament.component.scss']
})


export class SearchMedicamentComponent implements OnInit {
  
  phrarmacies: Pharmacie[] = null;
  
  center: GpsCoordinate = {
    lat: 37.76342,
    lng: -122.42742,
  }
 
  

  
  addForm: FormGroup;
  medicineToAdd: Medicament;
  medicinesAutoComplete: Medicament[] = [];
  selectedMedicament: Medicament;

  constructor(private pharmacieService: PharmacieService, private medicineService: MedicamentService,
    private formBuilder: FormBuilder,
    private stockService: StockService,
    private router: Router
  ) {

  }

  ngOnInit() {
    this.initForm()
    this.pharmacieService.getAllPharmacie().subscribe(ph => {
      this.phrarmacies = ph;
    });
  }
  initForm() {
    this.addForm = this.formBuilder.group({
      med: ['']
    })
    this.addForm.controls.med.valueChanges.subscribe(v => {
      if (v.id) {

        this.selectedMedicament = v;
        this.stockService.filterPharmacieByMedicament(this.selectedMedicament).subscribe(p => {
          this.phrarmacies = p;
        });
      }
      else {
        this.search(v);
      }

    })
  }

  search(char: string) {
    console.log('change', char);

    this.medicinesAutoComplete = [];
    this.medicinesAutoComplete = [];
    this.medicineService.search(char).subscribe(m => {
      console.log('m', m);

      this.medicinesAutoComplete = m;
    });

  }
  displayFn(med?: Medicament): string | undefined {
    return med ? med.name : undefined;
  }
  showPharmacie(pharmacie: Pharmacie) {
    this.router.navigate(['/profile/' + pharmacie.id])

  }

}