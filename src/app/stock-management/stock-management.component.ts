import { TypeMedicament } from './../models/type-medicament';
import { MedicamentService } from './../services/medicament.service';
import { Medicament } from './../models/medicament';
import { Pharmacie } from './../models/pharmacie';
import { Stock } from './../models/stock';
import { StockService } from './../services/stock.service';
import { Component, OnInit } from '@angular/core';
import { AuthentificationService } from 'app/login/authentification.service';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-stock-management',
  templateUrl: './stock-management.component.html',
  styleUrls: ['./stock-management.component.scss']
})
export class StockManagementComponent implements OnInit {

  stock: Stock[];
  inputQuantity: number[] = []
  medicineToAdd: Medicament;
  medicinesAutoComplete: Medicament[] = [{
    id: 1,
    name: 'sdfsdf',
    nbUnite: 2,
    prix: 1515,
    type: TypeMedicament.COMPRIME,
    unite: 'dsffds'
  }];
  quantityToAdd: number;
  addForm: FormGroup;

  constructor(private stockService: StockService,
    private medicineService: MedicamentService,
    private authentificationService: AuthentificationService,
    private formBuilder: FormBuilder) {

  }
  ngOnInit() {
    this.addForm = this.formBuilder.group({
      med: ['']
    })
    this.addForm.controls.med.valueChanges.subscribe(v => {
      this.search(v);
    })
    this.stockService.getStock(this.authentificationService.getCurrentUser() as Pharmacie).subscribe(s => {
      this.stock = s;
    });

  }
  updateStock(stock: Stock, q: number) {
    this.stockService
      .updateStock(stock, stock.quantity + q)
      .subscribe(() => {
        stock.quantity = stock.quantity + q;
        this.inputQuantity = [];

      })

  }
  search(char: string) {
    console.log('change', char);

    this.medicinesAutoComplete = [];
    this.medicinesAutoComplete = [];
    this.medicineService.search(char).subscribe(m => {
      console.log('m', m);

      this.medicinesAutoComplete = m;
    });

  }
  addStock(medicine: Medicament, quantityToAdd: number) {
    console.log('loggg', medicine);

    this.stockService
      .addStock(this.authentificationService.getCurrentUser() as Pharmacie, medicine, quantityToAdd)
      .subscribe(s => {
        this.stock = s;
      });

  }
  select(option) {
    console.log('ddd', option);
    this.addForm.controls.med.setValue(option);
  }
  displayFn(med?: Medicament): string | undefined {
    return med ? med.name : undefined;
  }

}
